package edu.washington.mislagle.quizdroid;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Mike on 2/21/2015.
 */
public class TopicAdapter extends ArrayAdapter<Topic> {
    Context context;
    int layoutResourceID;
    ArrayList<Topic> data = null;

    public TopicAdapter(Context context, int layoutResourceID, ArrayList<Topic> data){
        super(context, layoutResourceID, data);
        this.context = context;
        this.layoutResourceID = layoutResourceID;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        TopicHolder holder = null;

        if(row == null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceID, parent, false);

            holder = new TopicHolder();
            holder.title = (TextView)row.findViewById(R.id.title);
            holder.descr = (TextView)row.findViewById(R.id.description);

            row.setTag(holder);
        } else {
            holder = (TopicHolder)row.getTag();
        }

        Topic topic = data.get(position);
        holder.title.setText(topic.getTitle());
        holder.descr.setText(topic.getShortDescription());

        return row;
    }

    static class TopicHolder{
        TextView title;
        TextView descr;
    }
}
