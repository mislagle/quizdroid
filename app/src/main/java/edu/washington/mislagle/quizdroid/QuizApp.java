package edu.washington.mislagle.quizdroid;

import android.app.Application;
import android.text.style.SuperscriptSpan;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class QuizApp extends Application {

    private TopicImplementation topics;
    private static QuizApp thisQuizApp;
    private static final String TAG = "QuizApp";

    public QuizApp(){
        if(thisQuizApp == null){
            thisQuizApp = this;
        } else{
            Log.e(TAG, "MULTIPLE INSTANCES OF QUIZAPP");
            throw new RuntimeException("Too Many QuizApps");
        }
    }

    public static QuizApp newQuizApp(){
        return thisQuizApp;
    }

    public void onCreate(){
        super.onCreate();
        Log.i(TAG, "LOADING QUIZAPP DATA");
        topics = new TopicImplementation();
    }

    public TopicRepository getTopics(){
        return topics;
    }
}
