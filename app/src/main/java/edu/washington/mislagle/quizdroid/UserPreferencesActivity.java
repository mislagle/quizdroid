package edu.washington.mislagle.quizdroid;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class UserPreferencesActivity extends ActionBarActivity{
    public static SharedPreferences sharedPreferences;
    public static PendingIntent pendingIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_preferences);

        final EditText url = (EditText)findViewById(R.id.json_url);
        final EditText interval = (EditText)findViewById(R.id.refresh_interval);
        Button save = (Button)findViewById(R.id.save);
        Button stop = (Button)findViewById(R.id.stop);


        sharedPreferences = this.getSharedPreferences("user_settings", MODE_WORLD_READABLE);

        url.setText(sharedPreferences.getString("url", "http://tednewardsandbox.site44.com/questions.json"));
        interval.setText(sharedPreferences.getString("interval", "1440"));

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor prefEditor = sharedPreferences.edit();
                if(url.getText() != null || !url.getText().toString().isEmpty()){
                    prefEditor.putString("url", url.getText().toString());
                }
                if(interval.getText() != null || !interval.getText().toString().isEmpty()){
                    prefEditor.putString("interval", interval.getText().toString());
                }
                prefEditor.commit();
                stopAlarm();
                startAlarm();
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopAlarm();
            }
        });

        Intent alarmIntent = new Intent(UserPreferencesActivity.this, AlarmReceiver.class);
        alarmIntent.putExtra("url", sharedPreferences.getString("url", "http://tednewardsandbox.site44.com/questions.json"));
        pendingIntent = PendingIntent.getBroadcast(UserPreferencesActivity.this, 0, alarmIntent, 0);
    }

    public void startAlarm(){
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        int interval = Integer.parseInt(sharedPreferences.getString("interval", "1440"))*60000;
        alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), interval, pendingIntent);
        Toast.makeText(this, sharedPreferences.getString("url", "default"), Toast.LENGTH_SHORT).show();
    }

    public void stopAlarm() {
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        Toast.makeText(this, "Stopping refresh", Toast.LENGTH_SHORT).show();
    }

}
