package edu.washington.mislagle.quizdroid;

import java.util.ArrayList;

/**
 * Created by Mike on 2/21/2015.
 */
public interface TopicRepository {
    public ArrayList<Topic> getTopics();
    public Topic getTopic(int topic);
    public Question getQuestion(int topic, int Question);
}
