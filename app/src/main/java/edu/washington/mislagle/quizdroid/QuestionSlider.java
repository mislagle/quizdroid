package edu.washington.mislagle.quizdroid;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.R.animator.*;
import android.view.View;


public class QuestionSlider extends ActionBarActivity{

//    public static Topic thisTopic;
//    public static int currentQuestion = 0;
//    public static int numCorrect = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_slider);

//        Intent dataIn = getIntent();
//        thisTopic = (Topic)dataIn.getSerializableExtra("topic");

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        TopicOverviewFragmentActivity fragment = new TopicOverviewFragmentActivity();
        fragmentTransaction.add(R.id.placeholderFragment, fragment);
        fragmentTransaction.commit();
    }

    public void nextQuestion(){
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.setCustomAnimations(R.animator.slide_left_enter, R.animator.slide_left_exit);

        QuestionFragment fragment = new QuestionFragment();
        fragmentTransaction.replace(R.id.placeholderFragment, fragment);
        fragmentTransaction.commit();
    }

    public void showAnswer(int checkedButtonID){
        MainActivity.theirAnswer = checkedButtonID;

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.setCustomAnimations(R.animator.slide_left_enter, R.animator.slide_left_exit);

        AnswerSummaryFragment fragment = new AnswerSummaryFragment();
        fragmentTransaction.replace(R.id.placeholderFragment, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_question_slider, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
