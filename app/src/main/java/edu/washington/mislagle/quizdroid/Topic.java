package edu.washington.mislagle.quizdroid;

import java.io.Serializable;
import java.util.*;

public class Topic implements Serializable{
    private ArrayList<Question> Questions;
    private String shortDescription;
    private String longDescription;
    private String Title;

    public Topic(String topicTitle, String shortDescription, String longDescription, ArrayList<Question> topicQuestions){
        Title = topicTitle;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        Questions = topicQuestions;
    }

    public String getTitle(){
        return Title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public ArrayList<Question> getQuestions(){
        return Questions;
    }

    public String toString(){
        return Title;
    }
}
