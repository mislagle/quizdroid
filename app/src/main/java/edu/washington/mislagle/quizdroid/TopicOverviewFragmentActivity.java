package edu.washington.mislagle.quizdroid;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class TopicOverviewFragmentActivity extends Fragment {

    private QuestionSlider thisQuestionSlider;

    public TopicOverviewFragmentActivity() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_topic_overview, container, false);

        TextView title = (TextView) view.findViewById(R.id.topic_title);
        TextView descr = (TextView) view.findViewById(R.id.topic_descr);
        Button begin = (Button) view.findViewById(R.id.btn_begin);

        Topic thisTopic = QuizApp.newQuizApp().getTopics().getTopic(MainActivity.thisTopic);

        title.setText(thisTopic.getTitle());
        descr.setText(thisTopic.getLongDescription());

        begin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thisQuestionSlider.nextQuestion();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        thisQuestionSlider = (QuestionSlider) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}
