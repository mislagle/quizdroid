package edu.washington.mislagle.quizdroid;

/**
 * Created by Mike on 2/21/2015.
 */
public class TopicRow {
    public String Title;
    public String Descr;

    public TopicRow(){
        super();
    }

    public TopicRow(String title, String descr){
        super();
        Title = title;
        Descr = descr;
    }


}
