package edu.washington.mislagle.quizdroid;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import java.util.*;

import static edu.washington.mislagle.quizdroid.QuizApp.*;


public class MainActivity extends ActionBarActivity {

    public static int thisTopic;
    public static int currentQuestion = 0;
    public static int numCorrect = 0;
    public static int theirAnswer;


//    public static TopicRepository topics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView questionCategoriesView = (ListView) findViewById(R.id.listview_QuestionCategories);

        TopicAdapter CategoryAdapter = new TopicAdapter(this, R.layout.listview_item_row, QuizApp.newQuizApp().getTopics().getTopics());
        questionCategoriesView.setAdapter(CategoryAdapter);

        questionCategoriesView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                thisTopic = (Topic) parent.getItemAtPosition(position);
                thisTopic = position;

                Intent detailsData = new Intent(MainActivity.this, QuestionSlider.class);
                detailsData.putExtra("topic", thisTopic);

                if (detailsData.resolveActivity(getPackageManager()) != null) {
                    startActivity(detailsData);
                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(getApplicationContext(), UserPreferencesActivity.class);
            startActivity(settingsIntent);
        }



        return super.onOptionsItemSelected(item);
    }

    public void stopAlarm() {
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(UserPreferencesActivity.pendingIntent);
        Toast.makeText(this, "Stopping refresh", Toast.LENGTH_SHORT).show();
    }

    public void onDestroy(){
        stopAlarm();
    }
}
