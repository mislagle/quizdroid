package edu.washington.mislagle.quizdroid;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Mike on 2/21/2015.
 */
public class TopicImplementation implements TopicRepository {

    private ArrayList<Topic> topics;

    public TopicImplementation(){
        topics = new ArrayList<Topic>();

        ArrayList<Question> mathQs = new ArrayList<Question>();
        mathQs.add(new Question("2+2=", "4", "8", "22", "23", 1));
        mathQs.add(new Question("2*2=", "4", "8", "22", "23", 1));
        Topic Math = new Topic(
                "Math",
                "This is about numbers and what have you",
                "Mathematics is a great thing. You can do all sorts of cools stuff.",
                mathQs
                );

        ArrayList<Question> physQs = new ArrayList<Question>();
        physQs.add(new Question("Down or up?", "It's relative, man...", "Down", "Up", "Both", 1));
        physQs.add(new Question("What is the air speed velocity of an unlaiden swallow?", "African or European swallow?", "What? I don't know that...", "Ahhhhhh", "Blue! No -- yellow!", 1));
        Topic Physics = new Topic(
                "Physics",
                "Why shit works the way it does",
                "Physics helped us achieve great things like space flight and bigger bombs.",
                physQs
        );

        ArrayList<Question> superQs = new ArrayList<Question>();
        superQs.add(new Question("Who is your favorite super hero?", "Mike Slagle", "Matt Slagle", "Dan Slagle", "Patty Slagle", 4));
        superQs.add(new Question("Let me ask you again: Who is your favorite super hero?", "Mike Slagle", "Matt Slagle", "Dan Slagle", "Patty Slagle", 4));
        Topic SuperHeroes = new Topic(
                "Super Heroes",
                "These are extraordinary people who do good things.",
                "Super heroes are often more intelligent, stronger, have better hearts, and/or have super powers, and they use them to help people.",
                superQs
        );

        topics.add(Math);
        topics.add(Physics);
        topics.add(SuperHeroes);
    }

    public ArrayList<Topic> getTopics(){
        return topics;
    }

    public Topic getTopic(int topic){
        return topics.get(topic);
    }

    public Question getQuestion(int topic, int question){
        return topics.get(topic).getQuestions().get(question);
    }
}
