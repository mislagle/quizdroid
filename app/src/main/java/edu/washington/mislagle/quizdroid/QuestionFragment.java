package edu.washington.mislagle.quizdroid;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class QuestionFragment extends Fragment {

    private QuestionSlider thisQuestionSlider;

    public QuestionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question, container, false);

        Button submit = (Button) view.findViewById(R.id.btn_submit);
        TextView questionView = (TextView) view.findViewById(R.id.question);
        final RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        RadioButton option1 = (RadioButton) view.findViewById(R.id.option1);
        RadioButton option2 = (RadioButton) view.findViewById(R.id.option2);
        RadioButton option3 = (RadioButton) view.findViewById(R.id.option3);
        RadioButton option4 = (RadioButton) view.findViewById(R.id.option4);

        final Question thisQuestion = QuizApp.newQuizApp().getTopics().getQuestion(MainActivity.thisTopic, MainActivity.currentQuestion);

        questionView.setText(thisQuestion.getQuestionText());

        option1.setText(
                thisQuestion.getAnswer1()
        );
        option2.setText(
                thisQuestion.getAnswer2()
        );
        option3.setText(
                thisQuestion.getAnswer3()
        );
        option4.setText(
                thisQuestion.getAnswer4()
        );

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkedRadioButton = radioGroup.getCheckedRadioButtonId();

                if (checkedRadioButton != -1){

                    if(checkedRadioButton == R.id.option1) {
                        checkedRadioButton = 1;
                    } else if (checkedRadioButton == R.id.option2){
                        checkedRadioButton = 2;
                    } else if (checkedRadioButton == R.id.option3) {
                        checkedRadioButton = 3;
                    } else if (checkedRadioButton == R.id.option4) {
                        checkedRadioButton = 4;
                    }

                    if(thisQuestion.getCorrectAnswer() == checkedRadioButton){
                        MainActivity.numCorrect++;
                    }

                    thisQuestionSlider.showAnswer(checkedRadioButton);

                }
            }
        });

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        thisQuestionSlider = (QuestionSlider) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
