package edu.washington.mislagle.quizdroid;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class AnswerSummaryFragment extends Fragment {

    private QuestionSlider thisQuestionSlider;

    public AnswerSummaryFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_answer_summary, container, false);

        TextView correctView = (TextView) view.findViewById(R.id.numCorrect);
        Button next_finish = (Button) view.findViewById(R.id.btn_next_finish);
        TextView correctAnswer = (TextView) view.findViewById(R.id.correct_answer);
        TextView yourAnswer = (TextView) view.findViewById(R.id.your_answer);

        final int questionIndex = MainActivity.currentQuestion;
        final int totalQuestions = QuizApp.newQuizApp().getTopics().getTopic(MainActivity.thisTopic).getQuestions().size();
        Topic thisTopic = QuizApp.newQuizApp().getTopics().getTopic(MainActivity.thisTopic);
        Question thisQuestion = thisTopic.getQuestions().get(questionIndex);

        if(thisQuestion.getCorrectAnswer() == 1){
            correctAnswer.setText("Correct Answer: " + thisQuestion.getAnswer1());
        } else if(thisQuestion.getCorrectAnswer() == 2) {
            correctAnswer.setText("Correct Answer: " + thisQuestion.getAnswer2());
        } else if (thisQuestion.getCorrectAnswer() == 3){
            correctAnswer.setText("Correct Answer: " + thisQuestion.getAnswer3());
        } else if (thisQuestion.getCorrectAnswer() == 4) {
            correctAnswer.setText("Correct Answer: " + thisQuestion.getAnswer4());
        }

        if(MainActivity.theirAnswer == 1){
            yourAnswer.setText("Your Answer: " + thisQuestion.getAnswer1());
        } else if(MainActivity.theirAnswer == 2) {
            yourAnswer.setText("Your Answer: " + thisQuestion.getAnswer2());
        } else if (MainActivity.theirAnswer == 3){
            yourAnswer.setText("Your Answer: " + thisQuestion.getAnswer3());
        } else if (MainActivity.theirAnswer == 4) {
            yourAnswer.setText("Your Answer: " + thisQuestion.getAnswer4());
        }

        correctView.setText("You got " + MainActivity.numCorrect + " out of " + totalQuestions + " correct");

        if ((questionIndex + 1) == totalQuestions){
            next_finish.setText("Finish");
        }

        next_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            if ((questionIndex + 1) == totalQuestions){
                MainActivity.currentQuestion = 0;
                MainActivity.numCorrect = 0;
                Intent dataOut = new Intent(getActivity(), MainActivity.class);
                startActivity(dataOut);
            } else {
                MainActivity.currentQuestion++;
                thisQuestionSlider.nextQuestion();
            }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        thisQuestionSlider = (QuestionSlider) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
